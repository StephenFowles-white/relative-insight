import { createApp } from 'vue';
import { createStore } from 'vuex';
import Data from './data.json';
import App from './App.vue';

const app = createApp(App);

const store = createStore({
    state()  {
        return {
            data: Data.data,
        };
    },
    mutations: {
        increment (state, data) {
            const testlist = state.data;
            const newJsonData =  testlist.map(result => {
                if (result.id === data.id) {
                    const newLikeTotal = result.likes + 1
                    return {...result, 
                            likes: newLikeTotal}
                } else {
                    return result;
                }
            })
            state.data = newJsonData;
        },
        decrease (state, data) {
            const testlist = state.data;
            const newJsonData =  testlist.map(result => {
                if (result.id === data.id) {
                    const newLikeTotal = result.likes - 1
                    return {...result, 
                            likes: newLikeTotal}
                }
                else {
                    return result;
                }
            })
            state.data = newJsonData;
        },
        filter (state, data) {
            let newFilterdData = state.data;

            if (data.filter.filterType === 'rating') {
                if (data.filter.order === '' || data.filter.order === 'ascending') {
                    newFilterdData.sort((firstItem, secondItem) => firstItem.score - secondItem.score);
                }
                else {
                    newFilterdData.sort((firstItem, secondItem) => firstItem.score - secondItem.score).reverse();
                }

            } else if (data.filter.filterType === 'likes') {
                if (data.filter.order === '' || data.filter.order === 'ascending') {
                    newFilterdData.sort((firstItem, secondItem) => firstItem.likes - secondItem.likes);
                }
                else {
                    newFilterdData.sort((firstItem, secondItem) => firstItem.likes - secondItem.likes).reverse();
                }

            } else if (data.filter.filterType === 'submitted') {
                if (data.filter.order === '' || data.filter.order === 'ascending') {
                    newFilterdData.sort((firstItem, secondItem) => {
                        let firstDate = new Date(firstItem.submitted);
                        let secondDate = new Date(secondItem.submitted);
                        return firstDate - secondDate;
                    });
                }
                else {
                    newFilterdData.sort((firstItem, secondItem) => {
                        let firstDate = new Date(firstItem.submitted);
                        let secondDate = new Date(secondItem.submitted);
                        return firstDate - secondDate;
                    }).reverse();
                }
            }

            state.data = newFilterdData;
        }
    },
    actions: {
        increment (context, payload) {
            context.commit('increment', payload)
        },
        decrease (context, payload) {
            context.commit('decrease', payload)
        },
        filter (context, payload) {
            context.commit('filter', payload)
        }
    },
    getters: {
        getData(state) {
            return state.data;
        }
    }
})

app.use(store)
app.mount('#app')
