# relative-insight

# This app is designed to filter through a the json format provided for game reviews.

# The core functionality is
    - To display each of the game reviews from the data
    - To allow the user to filter the reviews based on when it is published, overall rating, and the games ( to allow users to quickly filter to a specific game if wanted)
    - The user also has the ability to like each of the review which changes the amount of likes.
    - The main chunk of filtering/sorting the data is in the Vuex store to try and keep all data manipulation in one place. ( this is not the case for reviewDataHandler() computed method in reviewList.vue)
    - set up the like options in the vuex store
    - when the user clicks the like button the image changes ( they only have the option to add one like to each review whilst on the app)

# Things I would do further:
    - I based the design base on the reviews having limited body text. However these reviews could have a lot more content in them. my solutions to this would be 
        1. Have a max character limit on the body and button on the card - once this is clicked it would open a full screen model with all the content.
        2. Move the grid to a single grid formate ( but i did think this could make the page very long)

    - I would make a few amends to the colours of the star but I think the overall concept relays the data well
    - If there was an API I would have linked the like vuex feature with the API to ensure that the data is updated for all users
    - I would also add a localstorage or cookies ( after concent given) to the brower with the like option so if the user was to go back onto the app the user would not be able to like the same review again.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

game
```
npm run build
```
### Compiles and hot-reloads for development

My appraoch was to begin with a vuex store to store and hold the data - this was to simlate the idea that a http reqest would most likily be in the vuex store and ensures that data is held in a constant location

I then broke the page donw into two section a header and a list of reviews section.

The list of review broke into two componenets, one been the article and the other been the over list container - it was on the list container that the getter for all the data is mounted onto the app.



